public class Calculator {
    public int addNosInString(String nosInString) {
        int sum =0 ;

        if(nosInString.isBlank())
            return sum;
        else if(nosInString.length()==1){
            return Integer.parseInt(nosInString);
        }
        else if(nosInString.startsWith("//")){
            String[] linesInString = nosInString.split("\n");
            String noInString = linesInString[1];
            return CalculateSum(sum, noInString);
        }
        else {
            return CalculateSum(sum, nosInString);
        }
    }

    private int CalculateSum(int sum, String nosInString) {
        String[] nos = getSplitedNos(nosInString);
        int num;
        for (int i = 0; i < nos.length; i++) {
            num = getAnInt(nos[i]);
            if(num<=99){
                sum = sum + num;
            }
        }
        return sum;
    }

    private String[] getSplitedNos(String noInString) {
        if(noInString.contains("-"))
            return noInString.split("\\W");
        else
            return noInString.split("[^\\d]+");
    }
    
    private int getAnInt(String string) {
        return Integer.parseInt(string);
    }
}
