import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class CalculatorTest {
    private Calculator cal;

    @Before
    public void setUp() throws Exception {
        cal = new Calculator();
    }

    @Test
    public void addEmptyString(){
        assertEquals(0,cal.addNosInString(""));
    }

    @Test
    public void addBlankString(){
        assertEquals(0,cal.addNosInString(" "));
    }

    @Test
    public void addSingleNoInString(){
        assertEquals(1, cal.addNosInString("1"));
    }

    @Test
    public void addTwoNosInString(){
        assertEquals(3,cal.addNosInString("1,2"));
    }

    @Test
    public void addMultipleNosInString(){
        assertEquals(10,cal.addNosInString("1,2,3,4"));
    }


    @Test
    public void addMultipleNosWithNewLineInString(){
        assertEquals(6,cal.addNosInString("1 \n 2,3"));
    }

    @Test
    public void addMultipleNosWithDiffDdelimiters(){
        assertEquals(13,cal.addNosInString("1@3$4\n5"));
    }

    @Test
    public void addMultilpleNosWithSpecifiedDelimiters(){
        assertEquals(6,cal.addNosInString("//:\n1:2:3"));
    }


    @Test
    public void  addMultipleNosWithSpecifiedMultiLengthDelimiters(){
        assertEquals(6,cal.addNosInString("//[***]\n1***2***3"));
    }

    @Test
    public void  addMultipleNosWithSpecifiedDiffDelimiters(){
        assertEquals(6,cal.addNosInString("//[*][#]\n1*2#3"));
    }

    @Test
    public void addMultipleNosWithSpecifiedMultiLengthDiffDelimiters(){
        assertEquals(6, cal.addNosInString("//[***][##]\n1***2##3"));
    }

    @Test
    public void addNegativeNos(){
        try {
            cal.addNosInString("1,-2,3");
            fail("Inavlid case");
        }
        catch (RuntimeException runtimeException){
            System.out.println("Negatives are not allowed");
        }
    }

    @Test
    public void addGreaterNos(){
        assertEquals(2,cal.addNosInString("2,1002"));
    }

    @Test
    public void addGreaterNosWithDifferentDelimiter(){
        assertEquals(5,cal.addNosInString("2@102%3"));
    }

}